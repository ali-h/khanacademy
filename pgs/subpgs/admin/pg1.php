<div class="pageheading">
	<span class="pgheading">
		<i class="fa fa-user fa-fw"></i>
		<span>All Users</span>
	</span>
</div>
<center>
	<hr class="bodyhr">
</center>
	<div class="tablediv">
			<span class="chartheading otherheading">List of All Registed Users</span>
			<div class="tableedge">
				<table class="listtable" cellspacing="0px" cellpadding="10px">
					<thead>
						<tr>
							<th>USER ID</th>
							<th class="imgth"></th>
							<th>NAME</th>
							<th>PROFESSION</th>
							<th>STATUS</th>
							<th>REGESTRATION DATE</th>
							<th>ACTIONS</th>
						</tr>
					</thead>
					<tbody>
						<tr class="ctr">
							<td>0001</td>
							<td char="imgth"><img class="userimg tb" src="Assets/defaultavatar.png"></td>
							<td>Ali Hassan</td>
							<td>Student</td>
							<td><b class="psym y">INACTIVE</b></td>
							<td>7/5/2018</td>
							<td class="actd">
									<i title="Show Complete Profile" class="fa fa-user-circle acicon"></i>
									<i title="Edit Info" class="fa fa-pencil-alt acicon"></i>
								</div>
							</td>
						</tr>
						<tr class="ctr">
							<td>0001</td>
							<td char="imgth"><img class="userimg tb" src="Assets/defaultavatar.png"></td>
							<td>Ali Hassan</td>
							<td>Student</td>
							<td><b class="psym y">INACTIVE</b></td>
							<td>7/5/2018</td>
							<td class="actd">
									<i title="Show Complete Profile" class="fa fa-user-circle acicon"></i>
									<i title="Edit Info" class="fa fa-pencil-alt acicon"></i>
								</div>
							</td>
						</tr>
						<tr class="ctr">
							<td>0001</td>
							<td char="imgth"><img class="userimg tb" src="Assets/defaultavatar.png"></td>
							<td>Ali Hassan</td>
							<td>Other Staff</td>
							<td><b class="psym ok">ACTIVE</b></td>
							<td>7/5/2018</td>
							<td class="actd">
									<i title="Show Complete Profile" class="fa fa-user-circle acicon"></i>
									<i title="Edit Info" class="fa fa-pencil-alt acicon"></i>
								</div>
							</td>
						</tr>
						<tr class="ctr">
							<td>0001</td>
							<td char="imgth"><img class="userimg tb" src="Assets/defaultavatar.png"></td>
							<td>Ali Hassan</td>
							<td>Teacher</td>
							<td><b class="psym rd">RETIRED/PASSED</b></td>						
							<td>7/5/2018</td>
							<td class="actd">
									<i title="Show Complete Profile" class="fa fa-user-circle acicon"></i>
									<i title="Edit Info" class="fa fa-pencil-alt acicon"></i>
								</div>
							</td>
						</tr>
						<tr class="ctr">
							<td>0001</td>
							<td char="imgth"><img class="userimg tb" src="Assets/defaultavatar.png"></td>
							<td>Ali Hassan</td>
							<td>Student</td>
							<td><b class="psym ok">ACTIVE</b></td>
							<td>7/5/2018</td>
							<td class="actd">
									<i title="Show Complete Profile" class="fa fa-user-circle acicon"></i>
									<i title="Edit Info" class="fa fa-pencil-alt acicon"></i>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
	</div>
<div class="extrasp"></div>