<div class="pageheading">
	<span class="pgheading">
		<i class="fa fa-bullseye fa-fw"></i>
		<span>Overview</span>
	</span>
</div>
<center>
	<hr class="bodyhr">
</center>
<div class="front">
	<div class="overview-wr">
		<div class="card-wr">
			<div class="card">
				<div class="incardup">
					<span class="cardnumber">
						<mark class="number">522&nbsp;</mark>
					</span>
				</div>
				<div class="incard">
					<span class="cardproperty">
						<mark class="property">Total Registerations</mark>
					</span>
				</div>
			</div>
		</div>
		<div class="card-wr">
			<div class="card">
				<div class="incardup">
					<span class="cardnumber">
						<mark class="number">112&nbsp;</mark>
					</span>
				</div>
				<div class="incard">
					<span class="cardproperty">
						<mark class="property">Students (Active)</mark>
					</span>
				</div>
			</div>
		</div>
		<div class="card-wr">
			<div class="card">
				<div class="incardup">
					<span class="cardnumber">
						<mark class="number">7&nbsp;</mark>
					</span>
				</div>
				<div class="incard">
					<span class="cardproperty">
						<mark class="property">Teachers (Active)</mark>
					</span>
				</div>
			</div>
		</div>
		<div class="card-wr">
			<div class="card">
				<div class="incardup">
					<span class="cardnumber">
						<mark class="number">6&nbsp;</mark>
					</span>
				</div>
				<div class="incard">
					<span class="cardproperty">
						<mark class="property">Other Staff (Active)</mark>
					</span>
				</div>
			</div>
		</div>
	</div>

	<div class="chartsncontrol-wr">
		<div class="charts float">
			<div class="chartbody">
				<span class="chartheading">Courses Split</span>
				<div class="chartinfo">
					<span class="info" data-colo="rgb(244,67,54)" data-subn="DIT" data-stud="56" data-perc="38%">
						<i class="colorblock" style="background-color: rgb(244,67,54);"></i>
						<b>DIT</b>
					</span>
					<span class="info" data-colo="rgb(33,150,243)" data-subn="English Language" data-stud="40" data-perc="30%">
						<i class="colorblock" style="background-color: rgb(33,150,243);"></i>
						<b>English Language</b>
					</span>
					<span class="info" data-colo="rgb(0,150,136)" data-subn="CIT" data-stud="38" data-perc="18%">
						<i class="colorblock" style="background-color: rgb(0,150,136);"></i>
						<b>CIT</b>
					</span>
					<span class="info" data-colo="rgb(255,152,0)" data-subn="E-Commerce" data-stud="25" data-perc="8%">
						<i class="colorblock" style="background-color: rgb(255,152,0);"></i>
						<b>E-Commerce</b>
					</span>
					<span class="info" data-colo="rgb(76,175,80)" data-subn="Web Development" data-stud="1" data-perc="4%">
						<i class="colorblock" style="background-color: rgb(76,175,80);"></i>
						<b>Web Development</b>
					</span>
				</div>
				<div class="infobox">
					<span class="info"><i class="colorblock" style="background-color: rgb(244,67,54);"></i><b>DIT</b></span>
					<span class="info">Students: <b>56</b></span>
					<span class="info">Share: <b>38%</b></span>
				</div>
				<ul class="chart">
					<li class="chartli">
						<span class="chartbar" data-colo="rgb(0,150,136)" data-subn="CIT" data-stud="38" data-perc="18%" style="height:18%;background-color: rgb(0,150,136);"></span>
					</li>
					<li class="chartli">
						<span class="chartbar" data-colo="rgb(33,150,243)" data-subn="English Language" data-stud="40" data-perc="30%" style="height:30%;background-color: rgb(33,150,243);"></span>
					</li>
					<li class="chartli">
						<span class="chartbar" data-colo="rgb(244,67,54)" data-subn="DIT" data-stud="56" data-perc="38%" style="height:38%;background-color: rgb(244,67,54);"></span>
					</li>
					<li class="chartli">
						<span class="chartbar" data-colo="rgb(76,175,80)" data-subn="Web Development" data-stud="1" data-perc="4%" style="height:4%;background-color: rgb(76,175,80);"></span>
					</li>
					<li class="chartli">
						<span class="chartbar" data-colo="rgb(255,152,0)" data-subn="E-Commerce" data-stud="25" data-perc="8%" style="height:8%;background-color: rgb(255,152,0);"></span>
					</li>
				</ul>
			</div>
		</div>
		<div class="others">
			<div class="otherbody">
					<div class="buttons">
						<span class="chartheading otherheading">Quick Access</span>
						<button class="qbut float fs qb">Recent Fee Payments</button>
						<button class="qbut float fs qb">New Students</button>
						<button class="qbut float fs qb">Teachers List</button>
						<button class="qbut float fs qb">Staff List</button>
						<button class="qbut float fs qb m">Daily Report</button>
						<button class="qbut float fs qb m">Monthly Report</button>
					<div class="body2">
						<span class="chartheading otherheading">Control Options</span>
						<button class="qbut float fs">Petty Cash Log/Control</button>
						<button class="qbut float fs">Bank Statements</button>
						<button class="qbut float fs">Teachers Payments</button>
						<button class="qbut float fs">Fee Receipts</button>
						<button class="qbut float fs">Students Remarking</button>
						<button class="qbut float fs">Courses Control</button>
						<button class="qbut float fs m">More Options</button>
					</div>
					</div>
			</div>
		</div>
	</div>
</div>
<div class="reportdiv">
	<div class="reports r1 float">
		<div class="reportcardbody">
				<span class="chartheading otherheading">Daily Report <b class="psym showm">Detailed Reports</b></span>
				<div>
					<div class="cage">
						<span class="cageprop">Fees Collected Today</span>
						<span class="cageans numf">7000</span>
						<i class="fa addb fa-plus-circle fa-lg"></i>
					</div>
				</div>
				<div>
					<div class="cage">
						<span class="cageprop">Petty Cash Used</span>
						<span class="cageans numf">300</span>
						<i class="fa addb fa-plus-circle fa-lg"></i>
					</div>
				</div>
				<div>
				<div class="cage">
						<span class="cageprop tot">Total Saved Today</span>
						<span class="cageans numf">6700</span>
					</div>
				</div>
				<div>
				<div class="cage">
						<span class="cageprop">Report For</span>
						<span class="cageans numf">Wed, 17-April 2019</span>
					</div>
				</div>
		</div>
	</div>

	<div class="reports r2 float">
		<div class="reportcardbody">
				<span class="chartheading otherheading">Monthly Report <b class="psym showm">Detailed Reports</b></span>
				<div>
					<div class="cage">
						<span class="cageprop">Fees Collected</span>
						<span class="cageans numf">28000</span>
					</div>
				</div>
				<div>
					<div class="cage">
						<span class="cageprop">Petty Cash Used</span>
						<span class="cageans numf">2100</span>
					</div>
				</div>
				<div>
					<div class="cage">
						<span class="cageprop tot">Total Saved This Month</span>
						<span class="cageans numf">25900</span>
					</div>
				</div>
				<div class="cage">
						<span class="cageprop">Report For</span>
						<span class="cageans numf">April 2019</span>
					</div>
				</div>
		</div>
	</div>
</div>
<div class="tablediv">
		<span class="chartheading otherheading">Recent Fee Payments <b class="psym showm">Show All</b></span>
		<div class="tableedge float">
			<table class="listtable" cellspacing="0px" cellpadding="10px">
				<thead>
					<tr>
						<th>NAME</th>
						<th>COURSE</th>
						<th>FEES ID#</th>
						<th>FEE</th>
						<th>STATUS</th>
						<th>MONTH</th>
						<th>DATE</th>
						<th>ACTIONS</th>
					</tr>
				</thead>
				<tbody>
					<tr class="ctr">
						<td>Ali H</td>
						<td>DIT</td>
						<td class="numf">#15522</td>
						<td class="numf">1600</td>
						<td><b class="psym ok">PAID</b></td>
						<td>April</td>
						<td class="numf">17/4/2019</td>
						<td class="actd">
								<i title="Receipt" class="fa fa-receipt acicon"></i>
								<i title="User History" class="fa fa-book acicon"></i>
							</div>
						</td>
					</tr>
					<tr class="ctr">
						<td>Ali H</td>
						<td>DIT</td>
						<td class="numf">#15522</td>
						<td class="numf">1600</td>
						<td><b class="psym ok">PAID</b></td>
						<td>April</td>
						<td class="numf">17/4/2019</td>
						<td class="actd">
								<i title="Receipt" class="fa fa-receipt acicon"></i>
								<i title="User History" class="fa fa-book acicon"></i>
							</div>
						</td>
					</tr>
					<tr class="ctr">
						<td>Ali H</td>
						<td>DIT</td>
						<td class="numf">#15522</td>
						<td class="numf">1600</td>
						<td><b class="psym ok">PAID</b></td>
						<td>April</td>
						<td class="numf">17/4/2019</td>
						<td class="actd">
								<i title="Receipt" class="fa fa-receipt acicon"></i>
								<i title="User History" class="fa fa-book acicon"></i>
							</div>
						</td>
					</tr>
					<tr class="ctr">
						<td>Ali H</td>
						<td>DIT</td>
						<td class="numf">#15522</td>
						<td class="numf">1600</td>
						<td><b class="psym y">PAID</b></td>
						<td>April</td>
						<td class="numf">17/4/2019</td>
						<td class="actd">
								<i title="Receipt" class="fa fa-receipt acicon"></i>
								<i title="User History" class="fa fa-book acicon"></i>
							</div>
						</td>
					</tr>
					<tr class="ctr">
						<td>Ali H</td>
						<td>DIT</td>
						<td class="numf">#15522</td>
						<td class="numf">1600</td>
						<td><b class="psym ok">PAID</b></td>
						<td>April</td>
						<td class="numf">17/4/2019</td>
						<td class="actd">
								<i title="Receipt" class="fa fa-receipt acicon"></i>
								<i title="User History" class="fa fa-book acicon"></i>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
</div>
<div class="tablediv">
		<span class="chartheading otherheading">New Students <b class="psym showm">Show All</b></span>
		<div class="tableedge float">
			<table class="listtable" cellspacing="0px" cellpadding="10px">
				<thead>
					<tr>
						<th>USER ID</th>
						<th class="imgth"></th>
						<th>NAME</th>
						<th>COURSE</th>
						<th>ADMISSION FEE</th>
						<th>F-STATUS</th>
						<th>DURATION</th>
						<th>REGESTRATION DATE</th>
						<th>ACTIONS</th>
					</tr>
				</thead>
				<tbody>
					<tr class="ctr">
						<td>0001</td>
						<td char="imgth"><img class="userimg tb" src="Assets/defaultavatar.png"></td>
						<td>Ali Hassan</td>
						<td>Web Development</td>
						<td class="numf">1900</td>
						<td><b class="psym y">PAID</b></td>
						<td>12 Months</td>
						<td>7/5/2018</td>
						<td class="actd">
								<i title="Show Complete Profile" class="fa fa-user-circle acicon"></i>
								<i title="Edit Info" class="fa fa-pencil-alt acicon"></i>
							</div>
						</td>
					</tr>
					<tr class="ctr">
						<td>0001</td>
						<td char="imgth"><img class="userimg tb" src="Assets/defaultavatar.png"></td>
						<td>Ali Hassan</td>
						<td>Web Development</td>
						<td class="numf">1900</td>
						<td><b class="psym y">UNPAID</b></td>
						<td>12 Months</td>
						<td>7/5/2018</td>
						<td class="actd">
								<i title="Show Complete Profile" class="fa fa-user-circle acicon"></i>
								<i title="Edit Info" class="fa fa-pencil-alt acicon"></i>
							</div>
						</td>
					</tr>
					<tr class="ctr">
						<td>0001</td>
						<td char="imgth"><img class="userimg tb" src="Assets/defaultavatar.png"></td>
						<td>Ali Hassan</td>
						<td>Web Development</td>
						<td class="numf">1900</td>
						<td><b class="psym ok">PAID</b></td>
						<td>12 Months</td>
						<td>7/5/2018</td>
						<td class="actd">
								<i title="Show Complete Profile" class="fa fa-user-circle acicon"></i>
								<i title="Edit Info" class="fa fa-pencil-alt acicon"></i>
							</div>
						</td>
					</tr>
					<tr class="ctr">
						<td>0001</td>
						<td char="imgth"><img class="userimg tb" src="Assets/defaultavatar.png"></td>
						<td>Ali Hassan</td>
						<td>Web Development</td>
						<td class="numf">1900</td>
						<td><b class="psym ok">PAID</b></td>
						<td>12 Months</td>
						<td>7/5/2018</td>
						<td class="actd">
								<i title="Show Complete Profile" class="fa fa-user-circle acicon"></i>
								<i title="Edit Info" class="fa fa-pencil-alt acicon"></i>
							</div>
						</td>
					</tr>
					<tr class="ctr">
						<td>0001</td>
						<td char="imgth"><img class="userimg tb" src="Assets/defaultavatar.png"></td>
						<td>Ali Hassan</td>
						<td>Web Development</td>
						<td class="numf">1900</td>
						<td><b class="psym ok">PAID</b></td>
						<td>12 Months</td>
						<td>7/5/2018</td>
						<td class="actd">
								<i title="Show Complete Profile" class="fa fa-user-circle acicon"></i>
								<i title="Edit Info" class="fa fa-pencil-alt acicon"></i>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
</div>
<div class="tablediv">
		<span class="chartheading otherheading">Teachers List <b class="psym showm">Show All</b></span>
		<div class="tableedge float">
			<table class="listtable" cellspacing="0px" cellpadding="10px">
				<thead>
					<tr>
						<th>USER ID</th>
						<th class="imgth"></th>
						<th>NAME</th>
						<th>QUALIFICATION</th>
						<th>SALARY</th>
						<th>JOINING DATE</th>
						<th>ACTIONS</th>
					</tr>
				</thead>
				<tbody>
					<tr class="ctr">
						<td>0001</td>
						<td char="imgth"><img class="userimg tb" src="Assets/defaultavatar.png"></td>
						<td>Ali Hassan</td>
						<td>MBA</td>
						<td class="numf">6000</td>
						<td>7/5/2018</td>
						<td class="actd">
								<i title="Show Complete Profile" class="fa fa-user-circle acicon"></i>
								<i title="Edit Info" class="fa fa-pencil-alt acicon"></i>
							</div>
						</td>
					</tr>
					<tr class="ctr">
						<td>0001</td>
						<td char="imgth"><img class="userimg tb" src="Assets/defaultavatar.png"></td>
						<td>Ali Hassan</td>
						<td>MBA</td>
						<td class="numf">6000</td>
						<td>7/5/2018</td>
						<td class="actd">
								<i title="Show Complete Profile" class="fa fa-user-circle acicon"></i>
								<i title="Edit Info" class="fa fa-pencil-alt acicon"></i>
							</div>
						</td>
					</tr>
					<tr class="ctr">
						<td>0001</td>
						<td char="imgth"><img class="userimg tb" src="Assets/defaultavatar.png"></td>
						<td>Ali Hassan</td>
						<td>MBA</td>
						<td class="numf">6000</td>
						<td>7/5/2018</td>
						<td class="actd">
								<i title="Show Complete Profile" class="fa fa-user-circle acicon"></i>
								<i title="Edit Info" class="fa fa-pencil-alt acicon"></i>
							</div>
						</td>
					</tr>
					<tr class="ctr">
						<td>0001</td>
						<td char="imgth"><img class="userimg tb" src="Assets/defaultavatar.png"></td>
						<td>Ali Hassan</td>
						<td>MBA</td>
						<td class="numf">6000</td>
						<td>7/5/2018</td>
						<td class="actd">
								<i title="Show Complete Profile" class="fa fa-user-circle acicon"></i>
								<i title="Edit Info" class="fa fa-pencil-alt acicon"></i>
							</div>
						</td>
					</tr>
					<tr class="ctr">
						<td>0001</td>
						<td char="imgth"><img class="userimg tb" src="Assets/defaultavatar.png"></td>
						<td>Ali Hassan</td>
						<td>MBA</td>
						<td class="numf">6000</td>
						<td>7/5/2018</td>
						<td class="actd">
								<i title="Show Complete Profile" class="fa fa-user-circle acicon"></i>
								<i title="Edit Info" class="fa fa-pencil-alt acicon"></i>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
</div>
<div class="tablediv">
		<span class="chartheading otherheading">Staff List <b class="psym showm">Show All</b></span>
		<div class="tableedge float">
			<table class="listtable" cellspacing="0px" cellpadding="10px">
				<thead>
					<tr>
						<th>USER ID</th>
						<th class="imgth"></th>
						<th>NAME</th>
						<th>JOB</th>
						<th>QUALIFICATION</th>
						<th>SALARY</th>
						<th>JOINING DATE</th>
						<th>ACTIONS</th>
					</tr>
				</thead>
				<tbody>
					<tr class="ctr">
						<td>0001</td>
						<td char="imgth"><img class="userimg tb" src="Assets/defaultavatar.png"></td>
						<td>Ali Hassan</td>
						<td>Reception</td>
						<td>BSC (CS)</td>
						<td class="numf">6000</td>
						<td>7/5/2018</td>
						<td class="actd">
								<i title="Show Complete Profile" class="fa fa-user-circle acicon"></i>
								<i title="Edit Info" class="fa fa-pencil-alt acicon"></i>
							</div>
						</td>
					</tr>
					<tr class="ctr">
						<td>0001</td>
						<td char="imgth"><img class="userimg tb" src="Assets/defaultavatar.png"></td>
						<td>Ali Hassan</td>
						<td>Reception</td>
						<td>BSC (CS)</td>
						<td class="numf">6000</td>
						<td>7/5/2018</td>
						<td class="actd">
								<i title="Show Complete Profile" class="fa fa-user-circle acicon"></i>
								<i title="Edit Info" class="fa fa-pencil-alt acicon"></i>
							</div>
						</td>
					</tr>
					<tr class="ctr">
						<td>0001</td>
						<td char="imgth"><img class="userimg tb" src="Assets/defaultavatar.png"></td>
						<td>Ali Hassan</td>
						<td>Reception</td>
						<td>BSC (CS)</td>
						<td class="numf">6000</td>
						<td>7/5/2018</td>
						<td class="actd">
								<i title="Show Complete Profile" class="fa fa-user-circle acicon"></i>
								<i title="Edit Info" class="fa fa-pencil-alt acicon"></i>
							</div>
						</td>
					</tr>
					<tr class="ctr">
						<td>0001</td>
						<td char="imgth"><img class="userimg tb" src="Assets/defaultavatar.png"></td>
						<td>Ali Hassan</td>
						<td>Reception</td>
						<td>BSC (CS)</td>
						<td class="numf">6000</td>
						<td>7/5/2018</td>
						<td class="actd">
								<i title="Show Complete Profile" class="fa fa-user-circle acicon"></i>
								<i title="Edit Info" class="fa fa-pencil-alt acicon"></i>
							</div>
						</td>
					</tr>
					<tr class="ctr">
						<td>0001</td>
						<td char="imgth"><img class="userimg tb" src="Assets/defaultavatar.png"></td>
						<td>Ali Hassan</td>
						<td>Reception</td>
						<td>BSC (CS)</td>
						<td class="numf">6000</td>
						<td>7/5/2018</td>
						<td class="actd">
								<i title="Show Complete Profile" class="fa fa-user-circle acicon"></i>
								<i title="Edit Info" class="fa fa-pencil-alt acicon"></i>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
</div>
<div class="extrasp"></div>
