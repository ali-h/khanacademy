<div class="hs1-wr">
	<div class="hs1 left">
		<p class="para">We provide you various kind of Courses for Computer Science and English language which includes short Courses and Yearly Diplomas. We assure better serving of Students and teach them Practically and theoritically both. Our Teachers teach Students with Proper Sallybus and welcoming Conditions. These are Courses avaliable now, for further information you may <a href="http://kacsel.ml">Contact Khan Academy</a> or <a href="http://kacsel.ml">Visit Us</a>.</p>
	</div>
</div>
<div class="hs1-wr">
	<div class="hs1 cs2">
		<div class="rowtiles rowt2">
					<div data-num="0" class="coursetile tilebig" style="background-color: rgb(244,67,54);">
						<div class="coursetilering" style="border-color: rgb(244,67,54);"></div>
						<p class="coursen"><i class="fa fa-microchip fa-4x"></i>DIT</p>
					</div>

					<div data-num="1" class="coursetile tilebig" style="background-color: rgb(33,150,243);">
						<div class="coursetilering" style="border-color: rgb(33,150,243);"></div>
						<p class="coursen"><i class="fa fa-language fa-4x"></i>English Language</p>
					</div>


					<div data-num="2" class="coursetile tilebig" style="background-color: rgb(0,150,136);">
						<div class="coursetilering" style="border-color: rgb(0,150,136);"></div>
						<p class="coursen"><i class="fa fa-keyboard fa-4x"></i>CIT</p>
					</div>

					<div data-num="3" class="coursetile tilebig" style="background-color: rgb(255,152,0);">
						<div class="coursetilering" style="border-color: rgb(255,152,0);"></div>
						<p class="coursen"><i class="fa fa-shopping-cart fa-4x"></i>E-Commerce</p>
					</div>
		</div>
		<div class="expandable-tile">
			<div class="info-body">
				<div class="sepdiv" class="maintitle">
				<span class="heading" id="fullform_course"></span>
				<span class="heading" id="title_course"></span>
				</div>
				<div class="sepdiv">
				<span class="paragraph" id="longdesc_course"></span>
				</div>
				<div class="sepdiv">
					<span class="paragraph"><b>Subjects: </b></span>
				<span class="paragraph" id="subjects_course"></span>
				</div>
				<div class="sepdiv">
				<span class="paragraph"><b>Monthly Fees: </b></span>
				<span class="paragraph" id="fees_course"></span>
				</div>
			</div>
		</div>
		<div class="rowtiles rowt2">

					<div data-num="4" class="coursetile tilebig" style="background-color: rgb(76,175,80);">
						<div class="coursetilering" style="border-color: rgb(76,175,80);"></div>
						<p class="coursen"><i class="fa fa-laptop-code fa-4x"></i>Web Development</p>
					</div>


					<div data-num="5" class="coursetile tilebig" style="background-color: rgb(255,152,0);">
						<div class="coursetilering" style="border-color: rgb(255,152,0);"></div>
						<p class="coursen"><i class="fa fa-shopping-cart fa-4x"></i>E-Commerce</p>
					</div>


					<div data-num="6" class="coursetile tilebig" style="background-color: rgb(241,89,78);">
						<div class="coursetilering" style="border-color: rgb(241,89,78);"></div>
						<p class="coursen"><i class="fa fa-suitcase-rolling fa-4x"></i>Others</p>
					</div>

					<div data-num="7" class="coursetile tilebig" style="background-color: rgb(255,152,0);">
						<div class="coursetilering" style="border-color: rgb(255,152,0);"></div>
						<p class="coursen"><i class="fa fa-shopping-cart fa-4x"></i>E-Commerce</p>
					</div>
		</div>
		<div class="expandable-tile" class="maintitle">
			<div class="info-body">
				<div class="sepdiv" id="maintitle">
				<span class="heading" id="fullform_course"></span>
				<span class="heading" id="title_course"></span>
				</div>
				<div class="sepdiv">
				<span class="paragraph" id="longdesc_course"></span>
				</div>
				<div class="sepdiv">
					<span class="paragraph"><b>Subjects: </b></span>
				<span class="paragraph" id="subjects_course"></span>
				</div>
				<div class="sepdiv">
				<span class="paragraph"><b>Monthly Fees: </b></span>
				<span class="paragraph" id="fees_course"></span>
				</div>
			</div>
		</div>
		<div class="courses-explains">
	<coursetitle data-num="0">DIT</coursetitle>
	<fullform data-num="0">Diploma in information Technology</fullform>
	<longdesc data-num="0">This Course is for Advanced Diploma in Information Technology. Proper Papers are Held by SBTE (Sindh Board Technical Education), Karachi.</longdesc>
	<subjects data-num="0">ICT, Operating System, C++, Web Dev (HTML, PHP, CSS), Corel Draw, Photoshop, Core Hardware, Office Automation, MS Access (Database), MySQL, DOS & Inpage</subjects>
	<fees data-num="0">RS 1,600/-</fees>

	<coursetitle data-num="1">ENGLISH LANGUAGE</coursetitle>
	<fullform data-num="1"></fullform>
	<longdesc data-num="1">This Course is to Learn, Read & Speak English with Impressive Accent. Course is Provided By Professional Teacher with Physical Activities.</longdesc>
	<subjects data-num="1">ENGLISH (Language), English (Accent)</subjects>
	<fees data-num="1">RS 1,600/-</fees>

	<coursetitle data-num="2">CIT</coursetitle>
	<fullform data-num="2">CERTIFICATE IN INFORMATION TECHNOLOGY</fullform>
	<longdesc data-num="2">This is a Sort Course for Students to Learn and Practice Important subjects in IT, Like Microsoft Office.</longdesc>
	<subjects data-num="2">ICT, Microsoft Office, Photoshop</subjects>
	<fees data-num="2">RS 1,600/-</fees>

	<coursetitle data-num="3">WEB DEVELOPMENT</coursetitle>
	<fullform data-num="3"></fullform>
	<longdesc data-num="3">This is an Advanced Course in Web Development to Practice all kind of Languages used on Websites, Etc.</longdesc>
	<subjects data-num="3">HTML, CSS, PHP, MYSQL, Javascript, JQuery (Library), Bootstrap (Library)</subjects>
	<fees data-num="3">RS 1,600/-</fees>

	<coursetitle data-num="4">E-COMMERCE</coursetitle>
	<fullform data-num="4"></fullform>
	<longdesc data-num="4">This Course is Provided by Sindh Goverment (Shaheed Benazir Bhutto Program), Course is to Learn about E-Commerce online Businesses and Shops. This is a Free Course which gives Students Mothly Scolorship.</longdesc>
	<subjects data-num="4">E-Commerce, E-Commerce Website</subjects>
	<fees data-num="4">RS 0/- (1300 SCOLORSHIP)</fees>
</div>
	</div>
</div>
