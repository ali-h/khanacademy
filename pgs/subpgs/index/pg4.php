<div class="hs1-wr">
	<div class="hs1 left center">
    <span class="heading">GALLERY FOR EVENTS</span>
    <hr />
    <div class="element-row">
      <div class="element2">
        <div class="gal-pic">
          <img class="userpic" src="Assets/gal1.jpg">
        </div>
        <div class="name">
          <span class="user-post user-desc size2">Students in Computer Lab, Downstairs in the Academy 2019</span>
        </div>
      </div>
      <div class="element2">
        <div class="gal-pic">
          <img class="userpic" src="Assets/gal2.jpg">
        </div>
        <div class="name">
          <span class="user-post user-desc size2">Certificates and Laptop Distribution With WWF Trainees</span>
        </div>
      </div>
      <div class="element2">
        <div class="gal-pic">
          <img class="userpic" src="Assets/gal3.jpg">
        </div>
        <div class="name">
          <span class="user-post user-desc size2">Successful completion of Training of "Alternate Livelihood Options for the Youth of Indus Ecoregion"</span>
        </div>
      </div>
    </div>

    <div class="element-row">
      <div class="element2">
        <div class="gal-pic">
          <img class="userpic" src="Assets/gal1.jpg">
        </div>
        <div class="name">
          <span class="user-post user-desc size2">Students in Computer Lab, Downstairs in the Academy 2019</span>
        </div>
      </div>
      <div class="element2">
        <div class="gal-pic">
          <img class="userpic" src="Assets/gal2.jpg">
        </div>
        <div class="name">
          <span class="user-post user-desc size2">Certificates and Laptop Distribution With WWF Trainees</span>
        </div>
      </div>
      <div class="element2">
        <div class="gal-pic">
          <img class="userpic" src="Assets/gal3.jpg">
        </div>
        <div class="name">
          <span class="user-post user-desc size2">Successful completion of Training of "Alternate Livelihood Options for the Youth of Indus Ecoregion"</span>
        </div>
      </div>
    </div>
	</div>
</div>
