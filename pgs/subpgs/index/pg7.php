<div class="login-div">
	<div class="panel cn">
		<p class="heading">ACCOUNT LOGIN</p>
		<input id="username" type="text" maxlength="32" class="minput" placeholder="Username">
		<input id="password" type="password" maxlength="32" class="minput pinput" placeholder="Password">
		<button id="loginb" class="minput subb">LOGIN</button>
		<p class="errmsg"> </p>
	</div>
	<div class="panel instructions">
	<p class="semhead">Instructions</p>
	<ul>
		<li>
			<p>You can login to your Account here by providing your Username and Password you choosed during Addmission.</p>
		</li>
		<li>
			<p>Only Students can login here.</p>
		</li>
		<li>
			<p>You Must be Registered and Approved by Admin and have must appeared in Academy to be able to Login here.</p>
		</li>
		<li>
			<p>Contact <a href="http://kacsel.ml">Khan Academy Support</a> to Report an Issue or Bug while Opening your Account. You can Contact Us by Mail or by Phone.</p>
		</li>
		<li>
			<p><a href="http://kacsel.ml">Click Here</a> for Addmission if you are new.</p>
		</li>
		<li>
			<p>Forgot Password? <a id="recover-anchor">Click Here</a> to Reset your Password.</p>
		</li>
	</ul>
	</div>
</div>
<div class="exspace"></div>
