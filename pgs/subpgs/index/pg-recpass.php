<div class="login-div">
	<div class="panel cn">
		<p class="heading">REST PASSWORD</p>
		<input id="username" type="text" class="minput" placeholder="Email">
		<button id="recb" class="minput subb">SUBMIT</button>
		<p class="errmsg"> </p>
	</div>
	<div class="panel instructions">
	<p class="semhead">Instructions</p>
	<ul>
		<li>
			<p>You can reset your Password by Entering your Email, if in case you Forgot or Lost old Password.</p>
		</li>
		<li>
			<p>This setup Will send you a Mail on your E-mail Guiding you to Reset your Password Easily.</p>
		</li>
		<li>
			<p>If Password Recovery setup Does not work or you have further Issues, you may contact your Teacher for Help.</p>
		</li>
	</ul>
	</div>
</div>
<div class="exspace"></div>