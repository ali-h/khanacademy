<div class="hs1-wr">
	<div class="hs1 left center">
    <span class="heading">THE TEAM</span>
    <hr />
    <div class="team-div">
			<div class="element-row">
	      <div class="element">
	        <div class="picture">
						<img class="userpic" src="Assets/teacher.jpg">
					</div>
					<div class="name">
						<span>Khan Muhammad</span>
						<span class="user-post">Administrator</span>
						<span class="user-post user-desc">Founder of Academy, Known for Computer Skills</span>
					</div>
	      </div>
				<div class="element">
	        <div class="picture">
						<img class="userpic" src="Assets/teacher.jpg">
					</div>
					<div class="name">
						<span>Muhammad Khan</span>
						<span class="user-post">Administrator</span>
						<span class="user-post user-desc">Founder of Academy, Known for English Skills and Knowladge</span>
					</div>
	      </div>
				<div class="element">
	        <div class="picture">
						<img class="userpic" src="Assets/teacher.jpg">
					</div>
					<div class="name">
						<span>Saeed Ahmed Khan</span>
						<span class="user-post">Senior Teacher</span>
						<span class="user-post user-desc">Skilled Teacher of Computer Science and Information Technology</span>
					</div>
	      </div>
		 </div>

		 <div class="element-row">
			 <div class="element">
				 <div class="picture">
					 <img class="userpic" src="Assets/teacher.jpg">
				 </div>
				 <div class="name">
					 <span>Khan Muhammad</span>
					 <span class="user-post">Administrator</span>
					 <span class="user-post user-desc">Founder of Academy, Known for Computer Skills</span>
				 </div>
			 </div>
			 <div class="element">
				 <div class="picture">
					 <img class="userpic" src="Assets/teacher.jpg">
				 </div>
				 <div class="name">
					 <span>Muhammad Khan</span>
					 <span class="user-post">Administrator</span>
					 <span class="user-post user-desc">Founder of Academy, Known for English Skills and Knowladge</span>
				 </div>
			 </div>
			 <div class="element">
				 <div class="picture">
					 <img class="userpic" src="Assets/teacher.jpg">
				 </div>
				 <div class="name">
					 <span>Saeed Ahmed Khan</span>
					 <span class="user-post">Senior Teacher</span>
					 <span class="user-post user-desc">Skilled Teacher of Computer Science and Information Technology</span>
				 </div>
			 </div>
		</div>
    </div>
	</div>
</div>
