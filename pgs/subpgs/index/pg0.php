<div class="hs1-wr fr-wr">
	<div class="fr">
		<div class="slideshow-home">
			<img class="slide-img" src="Assets/slide1.jpg"></img>
			<img class="slide-img" src="Assets/slide2.jpg" style="display:none;"></img>
			<img class="slide-img" src="Assets/slide3.jpg" style="display:none;"></img>
			<div class="pagination">
				<div class="slide2 slideac"></div>
				<div class="slide2"></div>
				<div class="slide2"></div>
			</div>
		</div>
	</div>
</div>
<p class="heading cn">OUR COURSES</p>
<div class="hs1-wr hse">
	<div class="hs1 cs">
		<div class="rowtiles">
					<div data-num="0" class="coursetile" style="background-color: rgb(244,67,54);">
						<div class="coursetilering" style="border-color: rgb(244,67,54);"></div>
						<p class="coursen"><i class="fa fa-microchip fa-3x"></i>DIT</p>
					</div>

					<div data-num="1" class="coursetile" style="background-color: rgb(33,150,243);">
						<div class="coursetilering" style="border-color: rgb(33,150,243);"></div>
						<p class="coursen"><i class="fa fa-language fa-3x"></i>English Language</p>
					</div>


					<div data-num="2" class="coursetile" style="background-color: rgb(0,150,136);">
						<div class="coursetilering" style="border-color: rgb(0,150,136);"></div>
						<p class="coursen"><i class="fa fa-keyboard fa-3x"></i>CIT</p>
					</div>

		</div>
		<div class="rowtiles">

					<div data-num="3" class="coursetile" style="background-color: rgb(76,175,80);">
						<div class="coursetilering" style="border-color: rgb(76,175,80);"></div>
						<p class="coursen"><i class="fa fa-laptop-code fa-3x"></i>Web Development</p>
					</div>


					<div data-num="4" class="coursetile" style="background-color: rgb(255,152,0);">
						<div class="coursetilering" style="border-color: rgb(255,152,0);"></div>
						<p class="coursen"><i class="fa fa-shopping-cart fa-3x"></i>E-Commerce</p>
					</div>


					<div data-num="5" class="coursetile" style="background-color: rgb(241,89,78);">
						<div class="coursetilering" style="border-color: rgb(241,89,78);"></div>
						<p class="coursen"><i class="fa fa-suitcase-rolling fa-3x"></i>Others</p>
					</div>
		</div>
	</div>
	<div class="hs1 cs inf">
		<div class="descdiv">
			<div class="courses" data-now="">
				<p class="heading sum">COURSE DESCRIPTION</p>
				<p class="sum2 maintitle"><span id="fullform_course"></span> <span id="title_course"></span></p>
				<p class="sum3" id="longdesc_course"></p>
				<p class="heading sum2">SUBJECTS: <span class="nb" id="subjects_course"></span></p>
				<p class="heading sum2">MONTHLY FEES: <span class="nb" id="fees_course"></span></p>
			</div>
		</div>
	</div>
	<div class="news-box">
		<div class="news-top">
			<span>NEWS</span>
		</div>
		<marquee scrollamount="4" class="news-marquee" direction="up" onmouseover="this.stop();" onmouseout="this.start();">
			<div class="news">
				<div class="news-inner">
					<span class="news-title"><a href="">DIT Addmission Open, Last Date 30 Jun, 2019</a></span>
				</div>
			</div>
			<div class="news">
				<div class="news-inner">
					<span class="news-title"><a href="">Acadmey will be close on Tuesday, Enjoy Pakistan vs India Match</a></span>
				</div>
			</div>
			<div class="news">
				<div class="news-inner">
					<span class="news-title"><a href="">New Assignments for the DIT Students Announced, Last Date of Submission 30 July, 2019</a></span>
				</div>
			</div>
			<div class="news">
				<div class="news-inner">
					<span class="news-title"><a href="">Result for DIT Batch January 2019 is Out</a></span>
				</div>
			</div>
			<div class="news">
				<div class="news-inner">
					<span class="news-title"><a href="">Addmissions for CIT New Batch are open, Last date of Submission Increased</a></span>
				</div>
			</div>
		</marquee>
	</div>
</div>
<div class="hs1-wr">
	<div class="hs1">
		<p class="heading">- OUR VISION -</p>
		<p class="para">Khan Academy of Computer Science and English Language prepares students to understand, contribute to, and succeed in a rapidly changing world, and thus become responsible, contributing members of society. We will ensure that our students develop the skills a sound in IT and Speaking Skills, and the competencies essential for success and Better future in the coming different World.</p>
	</div>
</div>

<div class="courses-explains">
	<coursetitle data-num="0">DIT</coursetitle>
	<fullform data-num="0">Diploma in information Technology</fullform>
	<longdesc data-num="0">This Course is for Advanced Diploma in Information Technology. Proper Papers are Held by SBTE (Sindh Board Technical Education), Karachi.</longdesc>
	<subjects data-num="0">ICT, Operating System, C++, Web Dev (HTML, PHP, CSS), Corel Draw, Photoshop, Core Hardware, Office Automation, MS Access (Database), MySQL, DOS & Inpage</subjects>
	<fees data-num="0">RS 1,600/-</fees>

	<coursetitle data-num="1">ENGLISH LANGUAGE</coursetitle>
	<fullform data-num="1"></fullform>
	<longdesc data-num="1">This Course is to Learn, Read & Speak English with Impressive Accent. Course is Provided By Professional Teacher with Physical Activities.</longdesc>
	<subjects data-num="1">ENGLISH (Language), English (Accent)</subjects>
	<fees data-num="1">RS 1,600/-</fees>

	<coursetitle data-num="2">CIT</coursetitle>
	<fullform data-num="2">CERTIFICATE IN INFORMATION TECHNOLOGY</fullform>
	<longdesc data-num="2">This is a Sort Course for Students to Learn and Practice Important subjects in IT, Like Microsoft Office.</longdesc>
	<subjects data-num="2">ICT, Microsoft Office, Photoshop</subjects>
	<fees data-num="2">RS 1,600/-</fees>

	<coursetitle data-num="3">WEB DEVELOPMENT</coursetitle>
	<fullform data-num="3"></fullform>
	<longdesc data-num="3">This is an Advanced Course in Web Development to Practice all kind of Languages used on Websites, Etc.</longdesc>
	<subjects data-num="3">HTML, CSS, PHP, MYSQL, Javascript, JQuery (Library), Bootstrap (Library)</subjects>
	<fees data-num="3">RS 1,600/-</fees>

	<coursetitle data-num="4">E-COMMERCE</coursetitle>
	<fullform data-num="4"></fullform>
	<longdesc data-num="4">This Course is Provided by Sindh Goverment (Shaheed Benazir Bhutto Program), Course is to Learn about E-Commerce online Businesses and Shops. This is a Free Course which gives Students Mothly Scolorship.</longdesc>
	<subjects data-num="4">E-Commerce, E-Commerce Website</subjects>
	<fees data-num="4">RS 0/- (1300 SCOLORSHIP)</fees>
</div>