<!DOCTYPE html>
<html>
<head>
	<title>KhanAcademy - Admin</title>
	<base href="//localhost/KhanAcademy/" />
	<meta id="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="CSS/dashboard.css">
	<script src="Tools/Jquery/jquery-min.js"></script>
	<script type="text/javascript" src="JS/admin.js"></script>
	<link rel="stylesheet" type="text/css" href="CSS/chart.css">
	<link href="https://fonts.googleapis.com/css?family=Muli:300" rel="stylesheet">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
</head>
<body>
	<header>
		<div class="bartop">
			<div class="pagetitlediv">
				<span><mark class="pagetitlemark">KHAN</mark> ACADEMY</span>
			</div>
		</div>
	</header>
	<aside>
		<div class="sidemenudiv">
			<div class="menu-wr">
				<div class="userinfo">
					<center>
						<img class="userimg" src="Assets/defaultavatar.png">
						<span class="username">Ali H (Admin)</span>
						<div style="margin-top: 5px;">
							<div class="iconquick">
								<i class="fa fa-bell"></i>
							</div>
							
							<div class="iconquick">
								<i class="fa fa-envelope"></i>
							</div>

							<div class="iconquick">
								<i class="fa fa-cog"></i>
							</div>
						</div>
						<hr class="sepaside">
					</center>
					<div class="menudiv">
						<div class="menubutton">
							<span>
								<i class="fa fa-bullseye fa-fw"></i>
								<span>Overview</span>
							</span>
						</div>
						<div class="menubutton">
							<span>
								<i class="fa fa-user fa-fw"></i>
								<span>All Users</span>
							</span>
						</div>
						<div class="menubutton">
							<span>
								<i class="fa fa-folder-open fa-fw"></i>
								<span>Reports</span>
							</span>
						</div>
						<div class="menubutton">
							<span>
								<i class="fa fa-microchip fa-fw"></i>
								<span>Control</span>
							</span>
						</div>
						<div class="menubutton">
							<span>
								<i class="fa fa-inbox fa-fw"></i>
								<span>Messages</span>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</aside>
	<div class="pagediv-wr">
		<div class="loader-wr">
			<div class="loading">
				<div class="loading-bar"></div>
				<div class="loading-bar"></div>
				<div class="loading-bar"></div>
				<div class="loading-bar"></div>
			</div>
		</div>
		<div id="pagediv"></div>
	</div>
</body>
</html>