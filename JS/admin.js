$(document).ready(function(){
	$(".menubutton").click(function(){
		var val = $(this).index();		
		aconcl(val);
	});
	function aconcl(val){
		sessionStorage.setItem("pg", val);
		if ($(".pagediv-wr").scrollTop() > 0) {
			$(".loader-wr").css({"position":"sticky"});
		}
		else{
			$(".loader-wr").css({"position":"absolute"});	
		}
		$(".loader-wr").show();
		ex_pg();
	}
	function ex_pg(){
		var pgnow = sessionStorage.getItem("pg");
		if (pgnow !== null) {
			loadpg(pgnow);
		}
		else{
			loadpg(0);
		}
	}
	ex_pg();	
	function loadpg(pgno) {
		var cu = $(".menubutton");
		cu.removeAttr("style");
			cu.eq(pgno).css({
				"background-color" : "#6c7ae0",
				"color" : "#fff"
		});
		cu.off();
		setTimeout(function () {
			$("#pagediv").load("pgs/subpgs/admin/pg"+pgno+".php", function() {
				cu.on("click", function(){
					var val = $(this).index();
					aconcl(val);
				});
				$(".loader-wr").hide();
				$(".info, .chartbar")
				.mouseover(function() {
					var color = $(this).attr("data-colo");;
					var subname = $(this).attr("data-subn");
					var quantity = $(this).attr("data-stud");
					var percentage = $(this).attr("data-perc");
					$(".infobox").find("b").eq(0).html(subname);
					$(".infobox").find("b").eq(1).html(quantity);
					$(".infobox").find("b").eq(2).html(percentage);					
					$(".infobox").find(".colorblock").css({"background-color":color});
					$(".infobox").show();
				})
				.mouseleave(function() {
					$(".infobox").hide();
				})
				if ($(".pagediv-wr").scrollTop() > 0) {
					$(".pagediv-wr").animate({ scrollTop: 0 }, "slow");
				}
				$(".qb").click(function(){
					var word = $(this).text()
					$(".otherheading").each(function(){
						if ($(this).html() == word+" <b class=\"psym showm\">Detailed Reports</b>") {

							$(".pagediv-wr").animate({ scrollTop: $(this).offset().top -90 }, "slow");							
						}
						else if ($(this).html() == word+" <b class=\"psym showm\">Show All</b>") {

							$(".pagediv-wr").animate({ scrollTop: $(this).offset().top -90 }, "slow");						
						}
					});
				});
			});
		}, 0);
	}
});