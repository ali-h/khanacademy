$(document).ready(function(){
	var homeslide;
	$(".menubutton").click(function(){
		var val = $(this).index();
		aconcl(val);
		intervals.forEach(clearInterval);
	});
	function aconcl(val){
		sessionStorage.setItem("pg", val);
		if ($(window).scrollTop() > 0) {
			$('html, body').animate({scrollTop : 0},"slow");
		}
		$(".loader-wr").show();
		ex_pg();
	}
	function ex_pg(){
		var pgnow = sessionStorage.getItem("pg");
		if (pgnow !== null) {
			loadpg(pgnow);
		}
		else{
			loadpg(0);
		}
	}
	ex_pg();
	function loadpg(pgno) {
		var cu = $(".menubutton");
		cu.removeClass("ac");
			cu.eq(pgno).addClass("ac");
		cu.off();
		setTimeout(function () {
			$("#pagediv").load("pgs/subpgs/index/pg"+pgno+".php", function() {
				$("input").on("keypress", function(e){
					if (e.keyCode == 13) {
						$("#loginb").click();
					}
				});
				$("#loginb").on("click", function(){
					var user = $("#username");
					var pass = $("#password");
					var errmsg = $(".errmsg");
					if (user.val() == "" || pass.val() == "") {
						errmsg.html("Username or Password can not be emputy.");
					}
					else{
						$(".loader-wr").show();
						inputstop(1);
						$.post("Actions/login.php",
						{
							username: user.val(),
							password: pass.val()
						},
						function(data, status){
							$(".loader-wr").hide();
							switch(data) {
								case "1":
								console.log("Access Granted");
								$(".loader-wr").show();
								break;
								case "err1":
								errmsg.html("Username does not Exist, Please try again.");
								inputstop(0);
								break;
								case "err2":
								errmsg.html("Wrong Password, Please try again.");
								inputstop(0);
								break;
							}
						});
					}
				});
				function inputstop(todo){
					if (todo == "1") {
						$("input").attr("disabled", "true");
						$("button").attr("disabled", "true");
					}
					else{
						$("input").removeAttr("disabled");
						$("button").removeAttr("disabled");
					}
				}
				cu.on("click", function(){
					var val = $(this).index();
					aconcl(val);
				});
				$(".loader-wr").hide();
				if (pgno == 0) {
				var max = 4;


					$(".coursetile").on("click", function() {
						var num = $(this).attr("data-num");
						$(".courses").attr("data-now", num);
						var color = $(this).css("background-color");
						setData(num, color);
						scalebig(num);
						clearInterval(courseslide);
					});

					function scalebig(num) {
						$(".coursetile").removeClass("tiled");
						$(".coursetile").eq(num).addClass("tiled")
					}

					$(".coursetile").eq(0).click();

					var courseslide = setInterval(function() {
						if (parseInt($(".courses").attr("data-now")) < max) {				
							var next = parseInt($(".courses").attr("data-now")) + 1;
							
						}
						else {
							var next = 0;
						}
						$(".courses").fadeOut(800, function(){
							$(".courses").attr("data-now", next);
							var color = $(".coursetile").eq(next).css("background-color");
							setData(next, color);
							scalebig(next);
							$(".courses").fadeIn(800);
						});
					},6000);

					homeslide = setInterval(function () {
						$(".slide-img").each(function () {
							if ($(this).css("display") == "block") {
								if ($(this).index() < 2) {
									var next = $(this).index() + 1;
								}
								else{
								 	var next = 0;
								}
								$(".slide2").removeClass("slideac");
								$(".slide2").eq(next).addClass("slideac");
								$(this).fadeOut(300, function () {
								$(".slide-img").eq(next).fadeIn(300);
								});
							}
						});
					},6000);

				}
				else if(pgno == 1){
					$(".coursetile").on("click", function() {
						expandtile(this.index());
					});
				}
				else if (pgno == 3) {
					$( ".tilebig" ).click(function() {
						var num = $(this).index();
						var color = $(this).css("background-color");
						var tile = $(this).parent().next(".expandable-tile");
						var body = $(this).parent().next(".expandable-tile").find(".info-body");
						if (tile.css("min-height") == "350px") {
							$(".tilebig").css({"transform": "scale(1)"});
							if (tile.find("#title_course").attr("data-num") == num) {
								tile.css({
									"-webkit-box-shadow": "none",
									"-moz-box-shadow":    "none",
									"box-shadow":         "none",
									"min-height": "0px",
									"max-height": "0px"
								});
								body.css({"display": "none"});
							}
							else{
								$(this).css({"transform": "scale(1.1)"});
								body.fadeOut("50", function () {
									setData(num);
									body.fadeIn("50");
								});
							}
						}
						else{
							$(this).css({"transform": "scale(1.1)"});
						$([document.documentElement, document.body]).animate({
				        scrollTop: tile.offset().top
				    }, "slow");
						var body = $(this).parent().next(".expandable-tile").find(".info-body");
						tile.css({
							"-webkit-box-shadow": "3px 3px 5px 6px rgb(108, 122, 224, 0.5)",
							"-moz-box-shadow":    "3px 3px 5px 6px rgb(108, 122, 224, 0.5)",
							"box-shadow":         "3px 3px 5px 6px rgb(108, 122, 224, 0.5)",
							"min-height": "350px",
							"max-height": "350px"
						});
						body.fadeIn("slow")
						setData(num, color);
					}
					});					
				}
				$("#recover-anchor").on("click", function(){
					aconcl("-recpass");
				});
			});
		}, 0);
	}
	function setData(num, color){
		var course = {
			title: $("coursetitle[data-num="+ num +"]").text(),
			fullform: $("fullform[data-num="+ num +"]").text(),
			longdesc: $("longdesc[data-num="+ num +"]").text(),
			subjects: $("subjects[data-num="+ num +"]").text(),
			fees: $("fees[data-num="+ num +"]").text()
		};
		$(".maintitle").css("color", color);
		$("#title_course").html("( " + course.title + " )");
		$("#fullform_course").html(course.fullform);
		$("#longdesc_course").html(course.longdesc);
		$("#subjects_course").html(course.subjects);
		$("#fees_course").html(course.fees);
		$("#title_course").attr("data-num", num);
		$("#fullform_course").attr("data-num", num);
		$("#longdesc_course").attr("data-num", num);
		$("#subjects_course").attr("data-num", num);
		$("#fees_course").attr("data-num", num);
	}
});
