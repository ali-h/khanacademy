<!DOCTYPE html>
<html>
<head>
	<title>Khan Academy</title>
	<base href="//localhost/KhanAcademy/" />
	<meta id="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="CSS/home.css">
	<script src="Tools/Jquery/jquery-min.js"></script>
	<script type="text/javascript" src="JS/home.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Muli:300" rel="stylesheet">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
</head>
<body>
	<div class="tops">
		<div class="topbar">
			<div class="barline"></div>
			<div class="links">
				<div class="linkscontent">
						<span class="timing">
							<i class="fa fa-clock sicons"></i>
							MON-SAT / 8:00 AM - 8:00 PM
						</span>
				</div>
			</div>
		</div>
		<div class="head">
			<div class="titlediv">
				<p class="h1"><mark class="cl">K</mark>HAN <mark class="cl">A</mark>CADEMY</p>
				<p class="h3 desc">A Place to Learn; A Chance to Grow!</p>
			</div>
			<div class="titlediv contacts">
				<div class="cbody">
					<p class="h2">Free Support!</p>
					<p class="h3 cl">support@kacsel.ml</p>
				</div>
				<div class="cbody sep"></div>
				<div class="cbody">
					<p class="h2">Contact Us!</p>
					<p class="h3 cl">+92 235 543355</p>
				</div>
			</div>
		</div>
		<div class="navbar">
			<nav>
				<div class="menubutton">
					<p>HOME</p>
				</div>
				<div class="menubutton">
					<p>ABOUT US</p>
				</div>
				<div class="menubutton">
					<p>ADDMISSION</p>
				</div>
				<div class="menubutton">
					<p>COURSES</p>
				</div>
				<div class="menubutton">
					<p>GALLERY</p>
				</div>
				<div class="menubutton">
					<p>FACULTY</p>
				</div>
				<div class="menubutton">
					<p>CONTACT US</p>
				</div>
				<div class="menubutton lgb">
					<p>STUDENT LOGIN</p>
				</div>
			</nav>
		</div>
	</div>
	<div class="pages">
		<div class="pagediv-wr">
			<div class="loader-wr">
				<div class="loading">
					<div class="loading-bar"></div>
					<div class="loading-bar"></div>
					<div class="loading-bar"></div>
					<div class="loading-bar"></div>
				</div>
			</div>
			<div id="pagediv"></div>
		</div>
	</div>
	<div class="footer-main">
		<div class="footer-inner">
			<span>Copyright © Khan Academy of Commputer Science and English Language @2019. All rights Reserved.</span>
		</div>
	</div>
</body>
</html>
