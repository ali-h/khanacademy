# Khan Academy

Khan Academy of Computer Science and English Language.
A place to Learn; A chance to Grow.

#### Repo Information:

This Repository is created for Website of Khan Academy of Computer Science and English Language on Wednesday, 15 May 2019. This includes All website files and Source Code.

#### Language Used:

PHP, HTML, CSS, Javascript And SQL (For Database).

#### Libraries/FrameWorks/Fonts Used:

Jquery, Google Fonts & Font Awesome (Icons).

###### Ali H. @2019 From Khan Academy Of Computer Science and English Language Sanghar. All Rights Reserved. Website Developed by Ali Hassan.